# Wilkommen im P1-Praktikum

In diesem Repository finden Sie einleitende Informationen zum Praktikum und Informationen zu allen angebotenen Versuchen des P1-Praktikums. 

## Einleitende Informationen zum Praktikum

- Wichtige Hinweise zum Arbeiten im Praktikum finden Sie in der Datei [ArbeitenImPraktikum.md](https://gitlab.kit.edu/kit/etp-lehre/p1-praktikum/students/-/blob/main/doc/ArbeitenImPraktikum.md).
- Praktische Hinweise zur Durchführung und Auswertung der Versuche finden Sie in der Datei [PraktischeHinweise.md](https://gitlab.kit.edu/kit/etp-lehre/p1-praktikum/students/-/blob/main/doc/PraktischeHinweise.md).
- Wichtige Hinweise zum Umgang mit dem Jupyter-Server der Fakultät finden Sie in der Datei [JupyterServer.md](https://gitlab.kit.edu/kit/etp-lehre/p1-praktikum/students/-/blob/main/doc/JupyterServer.md). Diese beinhaltet auch Hinweise zur Verwendung von [kafe2](https://gitlab.kit.edu/kit/etp-lehre/p1-praktikum/students/-/blob/main/doc/kafe2.md) oder [PhyPraKit](https://gitlab.kit.edu/kit/etp-lehre/p1-praktikum/students/-/blob/main/doc/PhyPraKit.md). 

## Informationen zu den angebotenen Versuchen des P1

In diesem Semester bieten wir die folgenden Versuche an: 

- [Vorversuch Datenverarbeitung](https://gitlab.kit.edu/kit/etp-lehre/p1-praktikum/students/-/tree/main/Vorversuch) (am Beispiel des Pendels)
- Elektrische Messverfahren 
-  Ferromagnetische Hysterese
- Schaltlogik
- [Vierpole und Leitungen](https://gitlab.kit.edu/kit/etp-lehre/p1-praktikum/students/-/tree/main/Vierpole_und_Leitungen)
- Oszilloskop
- [Resonanz](https://gitlab.kit.edu/kit/etp-lehre/p1-praktikum/students/-/tree/main/Resonanz)
- [Pendel](https://gitlab.kit.edu/kit/etp-lehre/p1-praktikum/students/-/tree/main/Pendel)
- Kreisel
- Elastizität
- Aeromechanik
- [Geometrische Optik](https://gitlab.kit.edu/kit/etp-lehre/p1-praktikum/students/-/tree/main/Geometrische_Optik)
- [Lichtgeschwindigkeit](https://gitlab.kit.edu/kit/etp-lehre/p1-praktikum/students/-/tree/main/Lichtgeschwindigkeit)
- [Spezifische Ladung des Elektrons](https://gitlab.kit.edu/kit/etp-lehre/p1-praktikum/students/-/tree/main/Spezifische_Ladung_des_Elektrons)

Die Struktur der Versuchsanleitungen ist in allen Versuchen gleich. Sobald Sie das Repository eines der Versuche betreten, finden Sie dort eine kurze Übersicht und Einführung zum Versuch. Diese Umfasst:

- Eine Motivation des Versuchs;
- Lehrziele;
- Bilder zum Versuch; 
- Verknüpfungen zu den Hinweisen zur Versuchsdurchführung und den für den Versuch relevanten Daten (in der jeweiligen Datei `Datenblatt.md`)

Die Übersicht und Einführung, sowie die verknüpften Hinweise sollten Sie in die Lage versetzen den Versuch durchzuführen. Die physikalischen Sachverhalte mit denen Sie sich während des Versuchs auseinandersetzen werden können Sie mit Hilfe einschlägiger Literatur weiter vertiefen. 

Die Versuchsdurchführung beginnen Sie mit dem Jupyter-notebook, das den Titel es Versuchs und die Endung **.ipynb* trägt auf dem Jupyter-Server der Fakultät. Fügen Sie dort die Text- und Code-Zellen ein, die Sie für das Protokoll der Versuchsdurchführung und die anschließende Auswertung benötigen.  Beachten Sie die [Praktischen Hinweise zur Durchführung und Auswertung der Versuche](https://gitlab.kit.edu/kit/etp-lehre/p1-praktikum/students/-/blob/main/doc/PraktischeHinweise.md). 
